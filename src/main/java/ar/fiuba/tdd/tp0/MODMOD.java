package ar.fiuba.tdd.tp0;

public class MODMOD implements IToken {

    @Override
    public void operate(ReversibleStack stack) {
        float acum;
        stack.invert();
        acum = stack.pop();
        while(!stack.isEmpty()) {
            acum = acum % stack.pop();
        }
        stack.push(acum);
    }
}
