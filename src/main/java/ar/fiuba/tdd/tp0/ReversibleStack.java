package ar.fiuba.tdd.tp0;


import java.util.Stack;

public class ReversibleStack {

    private Stack<Float> stack;

    public ReversibleStack() {
        stack = new Stack<>();
    }

    public Float pop() {
        return stack.pop();
    }

    public void push(Float num) {
        stack.push(num);
    }

    public void invert() {
        Stack<Float> auxstack = new Stack<>();
        while(!stack.isEmpty()) {
            auxstack.push(stack.pop());
        }
        stack = auxstack;
    }

    public boolean isEmpty() {
        return stack.isEmpty();
    }
}
