package ar.fiuba.tdd.tp0;


import java.util.HashMap;
import java.util.Map;

public class RPNCalculator {

    ReversibleStack stack;
    Map<String, IToken> tokenStrategy;

    public RPNCalculator() {
        stack = new ReversibleStack();
        tokenStrategy = new HashMap<String, IToken>();
        tokenStrategy.put("+", new Plus());
        tokenStrategy.put("-", new Minus());
        tokenStrategy.put("/", new Divide());
        tokenStrategy.put("*", new Multiply());
        tokenStrategy.put("MOD", new MOD());
        tokenStrategy.put("++", new PlusPlus());
        tokenStrategy.put("--", new MinusMinus());
        tokenStrategy.put("//", new DivideDivide());
        tokenStrategy.put("**", new MultiplyMultiply());
        tokenStrategy.put("MODMOD", new MODMOD());
    }

    public float eval(String expression) {

	try {
            for (String t : expression.split("\\s+")) {
                IToken token = tokenStrategy.get(t);
                if (token == null) {
                    stack.push(Float.parseFloat(t));
                } else {
                    token.operate(stack);
                }
            }
            return stack.pop();
        }
        catch (Exception e) {
            throw new IllegalArgumentException();
        }

    }
}
