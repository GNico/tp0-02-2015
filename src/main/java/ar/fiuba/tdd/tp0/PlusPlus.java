package ar.fiuba.tdd.tp0;


public class PlusPlus implements IToken {

    @Override
    public void operate(ReversibleStack stack) {
        float acum;
        stack.invert();
        acum = stack.pop();
        while(!stack.isEmpty()) {
            acum += stack.pop();
        }
        stack.push(acum);
    }
}
