package ar.fiuba.tdd.tp0;


public interface IToken {

    public void operate(ReversibleStack stack);
}
