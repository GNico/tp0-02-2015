package ar.fiuba.tdd.tp0;


public class Divide implements IToken {

    @Override
    public void operate(ReversibleStack stack) {
        float a = stack.pop();
        float b = stack.pop();
        stack.push(b / a);
    }
}
